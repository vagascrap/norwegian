//Norwegian scrapper One-Way

var cheerio = require('cheerio'),
_s = require('underscore.string'),
fs = require('fs'),
moment = require('moment');

//File name
var file = './Norwegian/norwegian.html';

var getTrip = function(data){

	$ = cheerio.load(data);
	
	var itinerary = {},
		returnTrip = [],
		trip = {};

		//Informacion sobre a hora de salida
		trip.departureInfoTime = moment($('table.layouttable>tbody>tr>td.layoutcell', data).text(), 'dddd Do. MMM YYYY').format('ddd, MMM DD,YYYY');
		
		//Hora de la salida
		trip.departureTime = moment($('td[class=depdest]>div', data).text(), 'hh:mma').format('h:mm A');

		//Tiempo en formato crudo
		trip.departureTimeRaw = moment(trip.departureInfoTime+' '+trip.departureTime,'ddd, MMM DD,YYYY hh:mm a').toISOString();

		//Ciudad de salida
		trip.departureCity = _s.clean($('tbody>tr:nth-child(2)>td.depdest>div', data).text());

		//Aeropuerto de salida
		trip.departureAirport = $('tr[class=top]>td.searchbarcell.filterendcell>select>option[selected=selected]', data).attr('value');

		//Tiempo de llegada
		trip.arrivalTime = moment($('td[class=arrdest]>div',data).text(),'hh:mma').format('h:mm A');

		//Aeropuerto de llegada
		trip.arrivalAirport =  $('tr[class=bottom]>td.searchbarcell.filterendcell>select>option[selected=selected]', data).attr('value');

		//Ciudad de llegada
		trip.arrivalCity = $('tbody>tr:nth-child(2)>td.arrdest>div',data).text();

		//Numero de paradas que habra en el viaje
		trip.stops = parseInt($('tbody>tr:nth-child(3)>td.depdest>div',data).text().replace(' ','').replace('stops',''));

		//Tiempo que durara el viaje
		trip.duration = $('td[class=duration]>div',data).text().replace('Duration: ','').replace(' ','');

		//Eliminar basura para dejar numeros solos
		var durArr = trip.duration
		.replace('h',' ')
		.replace('m',' ')
		.split(' ');

		//Objeto parseado para separar minutos, horas y días.
		trip.durationObj={};
		trip.durationObj.days = parseInt(durArr[0]/24); //Dias
		trip.durationObj.hours = durArr[0]%24;			//Horas
		trip.durationObj.mins = parseInt(durArr[1]);	//Minutos

		//Viaje operado por
		trip.operatedBy =  'Norwegian Air Shuttle ASA'; 

		//Nombre de la Aerolinea
		trip.airline= 'Norwegian Air Shuttle'; 
		
		//Codigo de la aerolinea.  El codigo es 00 si son aerolineas multiples
		trip.airLineCode = 'DY';

/*
		trip.mileage: '', //Número entero [opcional]. Incluir el número de millas si se cuenta con el dato
        trip.originTerminal: "", //[opcional] La terminal del aeropuerto donde saldrá
        trip.destinationTerminal: "", //[opcional] La terminal del aeropuerto donde llega
	    trip.meal: "" //[opcional] El tipo de comida que se sirve
*/

		returnTrip.push(trip);

	itinerary.trips = returnTrip;
	itinerary.totalCost = parseInt($('label.seatsokfare', data).text().replace(' ','').replace('$',''));
	itinerary.url = '';
	itinerary.provider = 'Norwegian';

	return [itinerary];
}

fs.readFile(file, {encoding: 'utf-8'}, function (err, data) {
	if (err) throw err;

	var itineraries = getTrip(data);
	console.log(JSON.stringify(itineraries));
});